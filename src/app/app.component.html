<div class="container">
<h1>TypeScript</h1>
<hr>
<p><i>From Wikipedia, the free encyclopedia</i></p>
<p>TypeScript is an open-source programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript, and adds optional static typing to the language.

TypeScript is designed for development of large applications and transcompiles to JavaScript.[5] As TypeScript is a superset of JavaScript, existing JavaScript programs are also valid TypeScript programs. TypeScript may be used to develop JavaScript applications for both client-side and server-side (Node.js) execution.

There are multiple options available for transcompilation. Either the default TypeScript Checker can be used,[6] or the Babel compiler can be invoked to convert TypeScript to JavaScript.[7]

TypeScript supports definition files that can contain type information of existing JavaScript libraries, much like C++ header files can describe the structure of existing object files. This enables other programs to use the values defined in the files as if they were statically typed TypeScript entities. There are third-party header files for popular libraries such as jQuery, MongoDB, and D3.js. TypeScript headers for the Node.js basic modules are also available, allowing development of Node.js programs within TypeScript.[8]

The TypeScript compiler is itself written in TypeScript and compiled to JavaScript. It is licensed under the Apache 2.0 License.

TypeScript is included as a first-class programming language in Microsoft Visual Studio 2013 Update 2 and later, beside C# and other Microsoft languages.[9] An official extension allows Visual Studio 2012 to support TypeScript as well.[10]

Anders Hejlsberg, lead architect of C# and creator of Delphi and Turbo Pascal, has worked on the development of TypeScript.</p>

<h3>History</h3>
<hr>
<p>TypeScript was first made public in October 2012 (at version 0.8), after two years of internal development at Microsoft.[15][16] Soon after the announcement, Miguel de Icaza praised the language itself, but criticized the lack of mature IDE support apart from Microsoft Visual Studio, which was not available on Linux and OS X at that time.[17][18] Today there is support in other IDEs, particularly in Eclipse, via a plug-in contributed by Palantir Technologies.[19][20] Various text editors, including Emacs, Vim, Sublime, Webstorm, Atom[21] and Microsoft's own Visual Studio Code also support TypeScript.[22]

TypeScript 0.9, released in 2013, added support for generics.[23] TypeScript 1.0 was released at Microsoft's Build developer conference in 2014.[24] Visual Studio 2013 Update 2 provides built-in support for TypeScript.[25]

In July 2014, the development team announced a new TypeScript compiler, claiming 5× performance gains. Simultaneously, the source code, which was initially hosted on CodePlex, was moved to GitHub.[26]

On 22 September 2016, TypeScript 2.0 was released; it introduced several features, including the ability for programmers to optionally prevent variables from being assigned null values,[27] sometimes referred to as the billion-dollar mistake.</p>

<h3>Language design</h3>
<hr>
<p>TypeScript originated from the shortcomings of JavaScript for the development of large-scale applications both at Microsoft and among their external customers.[28] Challenges with dealing with complex JavaScript code led to demand for custom tooling to ease developing of components in the language.[29]

TypeScript developers sought a solution that would not break compatibility with the standard and its cross-platform support. Knowing that the current ECMAScript standard proposal promised future support for class-based programming, TypeScript was based on that proposal. That led to a JavaScript compiler with a set of syntactical language extensions, a superset based on the proposal, that transforms the extensions into regular JavaScript. In this sense TypeScript was a preview of what to expect of ECMAScript 2015. A unique aspect not in the proposal, but added to TypeScript, is optional static typing[30] that enables static language analysis, which facilitates tooling and IDE support.</p>

<b>ECMAScript 2015 support</b>
<p><i>Main article: ECMAScript § 6th Edition - ECMAScript 2015</i></p>
<p>TypeScript adds support for features such as classes, modules, and an arrow function syntax as defined in the ECMAScript 2015 standard.</p>

<h3>Language features</h3>
<hr>
<p>TypeScript is a language extension that adds features to ECMAScript 6. Additional features include:</p>

<div class="lp">
<ul>
<li>Type annotations and compile-time type checking</li>
<li>Type inference</li>
<li>Type erasure</li>
<li>Interfaces</li>
<li>Enumerated types</li>
<li>Generics</li>
<li>Namespaces</li>
<li>Tuples</li>
<li>Async/await</li>
</ul>
</div>

<p>The following features are backported from ECMAScript 2015: </p>

<div class="lp">
<ul>
<li>Classes</li>
<li>Modules[31]</li>
<li>Abbreviated "arrow" syntax for anonymous functions</li>
<li>Optional parameters and default parameters</li>
</ul>
</div>

<p>Syntactically, TypeScript is very similar to JScript .NET, another Microsoft implementation of the ECMA-262 language standard that added support for static typing and classical object-oriented language features such as classes, inheritance, interfaces, and namespaces.</p>

<b>Compatibility with JavaScript</b>
<p>TypeScript is a strict superset of ECMAScript 2015, which is itself a superset of ECMAScript 5, commonly referred to as JavaScript.[32] As such, a JavaScript program is also a valid TypeScript program, and a TypeScript program can seamlessly consume JavaScript. By default the compiler targets ECMAScript 5, the current prevailing standard, but is also able to generate constructs used in ECMAScript 3 or 2015.

With TypeScript, it is possible to use existing JavaScript code, incorporate popular JavaScript libraries, and call TypeScript-generated code from other JavaScript.[33] Type declarations for these libraries are provided with the source code.</p>

<b>Type annotations</b>
<p>TypeScript provides static typing through type annotations to enable type checking at compile time. This is optional and can be ignored to use the regular dynamic typing of JavaScript.</p>

<p><code>
function add(left: number, right: number): number {{ '{' }} <br>
	return left + right; <br>
} <br>
</code></p>

<p>The annotations for the primitive types are number, boolean and string. Weakly- or dynamically-typed structures are of type any.

Type annotations can be exported to a separate declarations file to make type information available for TypeScript scripts using types already compiled into JavaScript. Annotations can be declared for an existing JavaScript library, as has been done for Node.js and jQuery.

The TypeScript compiler makes use of type inference to infer types when types are not given. For example, the add method in the code above would be inferred as returning a number even if no return type annotation had been provided. This is based on the static types of left and right being numbers, and the compiler's knowledge that the result of adding two numbers is always a number. However, explicitly declaring the return type allows the compiler to verify correctness.

If no type can be inferred because of lack of declarations, then it defaults to the dynamic any type. A value of the any type supports the same operations as a value in JavaScript and minimal static type checking is performed for operations on any values.[34]</p>

<b>Declaration files</b>
<p>When a TypeScript script gets compiled there is an option to generate a declaration file (with the extension .d.ts) that functions as an interface to the components in the compiled JavaScript. In the process the compiler strips away all function and method bodies and preserves only the signatures of the types that are exported. The resulting declaration file can then be used to describe the exported virtual TypeScript types of a JavaScript library or module when a third-party developer consumes it from TypeScript.

The concept of declaration files is analogous to the concept of header file found in C/C++. </p>

<p><code>
declare namespace arithmetics {{ '{' }} <br>
    add(left: number, right: number): number; <br>
    subtract(left: number, right: number): number; <br>
    multiply(left: number, right: number): number; <br>
    divide(left: number, right: number): number; <br>
} <br>
</code></p>

<p>Type declaration files can be written by hand for existing JavaScript libraries, as has been done for jQuery and Node.js.

Large collections of declaration files for popular JavaScript libraries are hosted on GitHub in DefinitelyTyped.</p>

<b>Classes</b>
<p>TypeScript supports ECMAScript 2015 classes that integrate the optional type annotations support.</p>

<p><code>
class Person {{ '{' }} <br>
    private name: string; <br>
    private age: number; <br>
    private salary: number; <br>
 <br>
    constructor(name: string, age: number, salary: number) {{ '{' }} <br>
        this.name = name; <br>
        this.age = age; <br>
        this.salary = salary; <br>
    } <br>
 <br>
    toString(): string {{ '{' }} <br>
        return `${{ '{' }}this.name} (${{ '{' }}this.age}) (${{ '{' }}this.salary})`; // As of version 1.4 <br>
    } <br>
} <br>
</code></p>

<b>Generics</b>
<p>TypeScript supports generic programming.[35]</p>

<p><code>
function doSomething{{ '\<\T>' }}arg: T): T {{ '{' }} <br>
    return arg; <br>
} <br>
</code></p>

<b>Modules and namespaces</b>
<p>TypeScript distinguishes between modules and namespaces. Both features in TypeScript support encapsulation of classes, interfaces, functions and variables into containers. Namespaces (formerly internal modules) utilize immediately-invoked function expression of JavaScript to encapsulate code, whereas modules (formerly external modules) leverage JavaScript library patterns to do so (AMD or CommonJS).[36]</p>

<h3>Development tools</h3>
<hr>
<b>Compiler</b>
<p>The TypeScript compiler, named tsc, is written in TypeScript. As a result, it can be compiled into regular JavaScript and can then be executed in any JavaScript engine (e.g. a browser). The compiler package comes bundled with a script host that can execute the compiler. It is also available as a Node.js package that uses Node.js as a host.

There is also an alpha version of a client-side compiler in JavaScript, which executes TypeScript code on the fly, upon page load.[37]

The current version of the compiler supports ECMAScript 5 by default. An option is allowed to target ECMAScript 2015 to make use of language features exclusive to that version (e.g. generators). Classes, despite being part of the ECMAScript 2015 standard, are available in both modes.</p>

<b>IDE and editor support</b>
<div class="lp">
<ul>
<li>Microsoft provides a plug-in for Visual Studio 2012 and WebMatrix, full integrated support in Visual Studio 2013, Visual Studio 2015, and basic text editor support for Sublime Text, Emacs and Vim.[38]</li>
<li>Visual Studio Code is a (mostly) open-source, cross-platform source code editor developed by Microsoft based on Electron. It supports TypeScript in addition to several other languages, and offers features like debugging and intelligent code completion.</li>
<li>alm.tools is an open source cloud IDE for TypeScript built using TypeScript, ReactJS and TypeStyle.</li>
<li>JetBrains supports TypeScript with code completion, refactoring and debugging in its IDEs built on IntelliJ platform, such as PhpStorm 6, WebStorm 6, and IntelliJ IDEA,[39] as well as their Visual Studio Add-in and extension, ReSharper 8.1.[40]</li>
<li>Atom has a TypeScript Plugin by Basarat with support for code completion, navigation, formatting, and fast compilation.</li>
<li>The online Cloud9 IDE and Codenvy support TypeScript.</li>
<li>A plugin is available for the NetBeans IDE.</li>
<li>A plugin is available for the Eclipse IDE (version Kepler)</li>
<li>TypEcs is available for the Eclipse IDE.</li>
<li>Microsoft provides a TypeScript Plugin for Sublime Text.</li>
<li>The Cross Platform Cloud IDE Codeanywhere supports TypeScript.</li>
<li>Webclipse An Eclipse plugin designed to develop TypeScript and Angular 2.</li>
<li>Angular IDE A standalone IDE available via npm to develop TypeScript and Angular 2 applications, with integrated terminal support.</li>
<li>Tide — TypeScript Interactive Development Environment for Emacs.</li>
<li>Tsuquyomi - a Vim plugin which uses TSServer and provides features like code completion, navigation and syntax and semantic error checking.</li>
</ul>
</div>

<b>Integration with build automation tools</b>
<p>Using plug-ins, TypeScript can be integrated with build automation tools, including Grunt (grunt-ts[41]), Apache Maven (TypeScript Maven Plugin[42]), Gulp (gulp-typescript[43]) and Gradle (TypeScript Gradle Plugin[44]).</p>

<b>Linting tools</b>
<p>TSLint[45] scans TypeScript code for conformance to a set of standards and guidelines. ESLint, a standard JavaScript linter, also provided some support for TypeScript via community plugins. However, ESLint's inability to leverage TypeScript's language services precluded certain forms of semantic linting and program-wide analysis.[46] In early 2019, the TSLint team announced the linter's deprecation in favor of typescript-eslint, a joint effort of the TSLint, ESLint and TypeScript teams to consolidate linting under the ESLint umbrella for improved performance, community unity and developer accessibility.[47]</p>
</div>